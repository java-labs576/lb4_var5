<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="my-tags" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
    <title>Lb 4</title>
</head>
<body>
<h1>Задание</h1>
<p>Разработать тег, который отображает текущие дату и время на сервере.</p>

<h1>Решение</h1>
<p>
    Время и дата на сервере: <br>
    <my-tags:time-display/>
</p>
</body>
</html>