<%@ tag pageEncoding="UTF-8" language="java" %>
<%@ tag import="java.text.SimpleDateFormat" %>

<%
    SimpleDateFormat dateFormat
            = new SimpleDateFormat("Время: HH:mm:ss Число: dd.MM.yyyy");
%>
<%= dateFormat.format(new java.util.Date()) %>